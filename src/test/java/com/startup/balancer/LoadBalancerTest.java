package com.startup.balancer;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class LoadBalancerTest {
    private static final int DEFAULT_NUMBER_OF_PARALLEL_CALLS = 1;
    private static final int DEFAULT_HEARTBEAT_INTERVAL_SEC = 1;
    private static final int LOAD_BALANCER_PROVIDERS_MAX_COUNT = 10;

    private LoadBalancer loadBalancer = new LoadBalancer();

    @BeforeEach
    public void clean() {
        loadBalancer = new LoadBalancer();
    }

    @Test
    public void register_whenRegisteredProvidersIsNull_shouldThrowIllegalArgumentException() {
        // given
        List<Provider> providersToRegister = null;

        // when
        Assertions.assertThrows(IllegalArgumentException.class, () -> loadBalancer.register(providersToRegister),
                "Register providers can not be null")
        ;
    }

    @Test
    public void register_whenRegisterProvidersMoreThanMaxAcceptedCount_shouldThrowIllegalStateException() {
        // given
        List<Provider> providersToRegister = Stream.generate(Provider::new)
                .limit(LOAD_BALANCER_PROVIDERS_MAX_COUNT + 1)
                .collect(Collectors.toList());

        // when
        var exception = Assertions.assertThrows(IllegalStateException.class, () -> loadBalancer.register(providersToRegister));

        // then
        Assertions.assertEquals("Trying to register 11 providers, balancer already contains 0 providers, limit is 10", exception.getMessage());
    }

    @Test
    public void register_whenRegisterProvidersLessThanMaxAcceptedCount_shouldReturnAllProviderIds() {
        // given
        List<Provider> providersToRegister = Stream.generate(Provider::new)
                .limit(new Random().nextInt(LOAD_BALANCER_PROVIDERS_MAX_COUNT))
                .collect(Collectors.toList());

        // when
        var success = loadBalancer.register(providersToRegister);

        // then
        Assertions.assertTrue(success);

        var registeredProviderIds = loadBalancer.getProvidersIds();
        Assertions.assertEquals(providersToRegister.size(), registeredProviderIds.size());
        IntStream.range(0, providersToRegister.size())
                .forEach(i -> Assertions.assertEquals(providersToRegister.get(i).getId(), registeredProviderIds.get(i)));
    }

    @Test
    public void get_whenRandomAlgorithmChosen_shouldGetRandomProviderId() {
        // given
        List<Provider> inputProviders = Stream.generate(Provider::new)
                .limit(10L)
                .collect(Collectors.toList());
        var loadBalancerWithRoundRobinAlgorithm = new LoadBalancer(DEFAULT_HEARTBEAT_INTERVAL_SEC, DEFAULT_NUMBER_OF_PARALLEL_CALLS, new RandomAlgorithm());
        loadBalancerWithRoundRobinAlgorithm.register(inputProviders);

        // when
        var randomId = loadBalancerWithRoundRobinAlgorithm.get();

        // then
        Assertions.assertTrue(inputProviders.stream().anyMatch(provider -> provider.getId().equals(randomId)));
    }

    @Test
    public void get_whenRoundRobinAlgorithmChosen_shouldGetSequentialProviderId() {
        // given
        List<Provider> inputProviders = Stream.generate(Provider::new)
                .limit(2L)
                .collect(Collectors.toList());
        var loadBalancerWithRoundRobinAlgorithm = new LoadBalancer(DEFAULT_HEARTBEAT_INTERVAL_SEC, DEFAULT_NUMBER_OF_PARALLEL_CALLS, new RoundRobinAlgorithm());
        loadBalancerWithRoundRobinAlgorithm.register(inputProviders);

        // when
        var sequentialId1 = loadBalancerWithRoundRobinAlgorithm.get();
        var sequentialId2 = loadBalancerWithRoundRobinAlgorithm.get();
        var sequentialId3 = loadBalancerWithRoundRobinAlgorithm.get();
        var sequentialId4 = loadBalancerWithRoundRobinAlgorithm.get();
        var sequentialId5 = loadBalancerWithRoundRobinAlgorithm.get();

        // then
        Assertions.assertAll("Check execution order:",
                () -> {
                    Assertions.assertEquals(inputProviders.get(0).getId(), sequentialId1);
                    Assertions.assertEquals(inputProviders.get(1).getId(), sequentialId2);
                    Assertions.assertEquals(inputProviders.get(0).getId(), sequentialId3);
                    Assertions.assertEquals(inputProviders.get(1).getId(), sequentialId4);
                    Assertions.assertEquals(inputProviders.get(0).getId(), sequentialId5);
                }
        );
    }

    @Test
    public void exclude_whenInputIsRegisteredProvider_shouldExcludeItFromRegisteredProviders() {
        // given
        List<Provider> inputProviders = Stream.generate(Provider::new)
                .limit(2L)
                .collect(Collectors.toList());
        var providerToExclude = inputProviders.get(1);
        loadBalancer.register(inputProviders);

        // when
        var success = loadBalancer.exclude(providerToExclude);

        // then
        Assertions.assertTrue(success);

        var providerIds = loadBalancer.getProvidersIds();
        Assertions.assertEquals(1, providerIds.size());
        Assertions.assertEquals(inputProviders.get(0).getId(), providerIds.get(0));
    }

    @Test
    public void exclude_whenInputIsNotRegisteredProvider_shouldNotExcludeItFromRegisteredProviders() {
        // given
        List<Provider> inputProviders = Stream.generate(Provider::new)
                .limit(2L)
                .collect(Collectors.toList());
        var providerToExclude = new Provider();
        loadBalancer.register(inputProviders);

        // when
        var success = loadBalancer.exclude(providerToExclude);

        // then
        Assertions.assertFalse(success);

        var providerIds = loadBalancer.getProvidersIds();
        Assertions.assertEquals(2, providerIds.size());
        Assertions.assertEquals(inputProviders.get(0).getId(), providerIds.get(0));
        Assertions.assertEquals(inputProviders.get(1).getId(), providerIds.get(1));
    }

    @Test
    public void include_whenRegisteredProvidersLessThanMaxAcceptedCount_shouldIncludeItToRegisteredProviders() {
        // given
        List<Provider> inputProviders = Stream.generate(Provider::new)
                .limit(2L)
                .collect(Collectors.toList());
        var providerToInclude = new Provider();
        loadBalancer.register(inputProviders);

        // when
        var success = loadBalancer.include(providerToInclude);

        // then
        Assertions.assertTrue(success);

        var providerIds = loadBalancer.getProvidersIds();
        Assertions.assertEquals(3, providerIds.size());
        Assertions.assertEquals(inputProviders.get(0).getId(), providerIds.get(0));
        Assertions.assertEquals(inputProviders.get(1).getId(), providerIds.get(1));
        Assertions.assertEquals(providerToInclude.getId(), providerIds.get(2));
    }

    @Test
    public void include_whenRegisteredProvidersEqualMaxAcceptedCount_shouldThrowIllegalStateException() {
        // given
        List<Provider> providersToRegister = Stream.generate(Provider::new)
                .limit(LOAD_BALANCER_PROVIDERS_MAX_COUNT)
                .collect(Collectors.toList());
        var providerToInclude = new Provider();
        loadBalancer.register(providersToRegister);

        // when
        var exception = Assertions.assertThrows(IllegalStateException.class, () -> loadBalancer.include(providerToInclude));

        // then
        Assertions.assertEquals("Trying to register 1 providers, balancer already contains 10 providers, limit is 10", exception.getMessage());
    }

}
