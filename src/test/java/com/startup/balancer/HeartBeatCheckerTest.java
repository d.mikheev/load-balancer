package com.startup.balancer;

import org.awaitility.Awaitility;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.Duration;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.mock;
import static org.easymock.EasyMock.replay;

public class HeartBeatCheckerTest {

    @Test
    public void shouldInvokeAllRegisteredProvidersEverySecond() {
        // given
        var heartBeatRateSec = 1;

        Provider provider1 = Mockito.mock(Provider.class);
        Provider provider2 = Mockito.mock(Provider.class);
        Mockito.when(provider1.check()).thenReturn(true);
        Mockito.when(provider2.check()).thenReturn(true);

        // when
        LoadBalancer loadBalancer = new LoadBalancer();
        loadBalancer.register(List.of(provider1, provider2));

        // then
        Awaitility.await()
                .atLeast(2, TimeUnit.SECONDS)
                .atMost(3500, TimeUnit.MILLISECONDS)
                .pollInterval(Duration.ofMillis(200))
                .untilAsserted(() -> {
                    Mockito.verify(provider1, Mockito.times(3)).check();
                    Mockito.verify(provider2, Mockito.times(3)).check();
                });
    }

    @Test
    public void shouldExcludeProviderFromLoadBalancer_whenProviderCheckWasFailed() {
        // given
        var heartBeatRateSec = 1;
        var provider1Id = UUID.randomUUID().toString();

        Provider provider1 = Mockito.mock(Provider.class);
        Provider provider2 = Mockito.mock(Provider.class);
        Mockito.when(provider1.check()).thenReturn(true);
        Mockito.when(provider1.getId()).thenReturn(provider1Id);
        Mockito.when(provider2.check()).thenReturn(false);

        // when
        LoadBalancer loadBalancer = new LoadBalancer();
        loadBalancer.register(List.of(provider1, provider2));

        // then
        Awaitility.await()
                .atMost(1, TimeUnit.SECONDS)
                .pollInterval(Duration.ofMillis(200))
                .untilAsserted(() -> {
                    Mockito.verify(provider2, Mockito.times(1)).check();
                    Assertions.assertEquals(1, loadBalancer.getProviders().size());
                    Assertions.assertEquals(provider1Id, loadBalancer.getProvidersIds().get(0));
                });
    }

    @Test
    public void shouldIncludeProviderToLoadBalancer_whenProviderCheckWasPassedTwoTimesInARow() {
        // given
        var heartBeatRateSec = 1;
        var provider1Id = UUID.randomUUID().toString();
        var provider2Id = UUID.randomUUID().toString();

        Provider provider1 = Mockito.mock(Provider.class);
        Mockito.when(provider1.check()).thenReturn(true);
        Mockito.when(provider1.getId()).thenReturn(provider1Id);

        Provider provider2 = mock(Provider.class);
        expect(provider2.getId()).andReturn(provider2Id).times(4);
        expect(provider2.check()).andReturn(false);
        expect(provider2.check()).andReturn(true);
        expect(provider2.check()).andReturn(true);
        expect(provider2.check()).andReturn(true);
        replay(provider2);

        // when
        LoadBalancer loadBalancer = new LoadBalancer();
        loadBalancer.register(List.of(provider1, provider2));

        // then
        Awaitility.await()
                .atMost(3, TimeUnit.SECONDS)
                .pollInterval(Duration.ofMillis(200))
                .untilAsserted(() -> {
                    Assertions.assertEquals(2, loadBalancer.getProviders().size());
                    Assertions.assertEquals(provider1Id, loadBalancer.getProvidersIds().get(0));
                    Assertions.assertEquals(provider2Id, loadBalancer.getProvidersIds().get(1));
                });
    }
}
