package com.startup.balancer;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RandomAlgorithmTest {

    private RandomAlgorithm randomAlgorithm = new RandomAlgorithm();

    @Test
    public void get_whenNoProviders_shouldThrowIllegalArgumentException() {
        // given
        List<Provider> inputProviders = Collections.emptyList();

        // when
        var exception = Assertions.assertThrows(IllegalArgumentException.class, () -> randomAlgorithm.get(inputProviders));

        // then
        Assertions.assertEquals("There are no providers to invoke", exception.getMessage());
    }

    @RepeatedTest(100)
    public void get_shouldGetRandomProviderId() {
        // given
        List<Provider> inputProviders = Stream.generate(Provider::new)
                .limit(1000L)
                .collect(Collectors.toList());

        // when
        var randomId = randomAlgorithm.get(inputProviders);

        // then
        Assertions.assertTrue(inputProviders.stream().anyMatch(provider -> provider.getId().equals(randomId)));
    }
}
