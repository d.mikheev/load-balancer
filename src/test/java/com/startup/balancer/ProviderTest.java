package com.startup.balancer;


import org.junit.jupiter.api.Test;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ProviderTest {

    @Test
    public void shouldGenerateProviderWithUniqueId() {
        // given
        final var PROVIDERS_TO_GENERATE = 10000;

        // when
        final var providersSet = Stream.generate(Provider::new)
                .limit(PROVIDERS_TO_GENERATE)
                .collect(Collectors.toSet());

        // then
        assertEquals(PROVIDERS_TO_GENERATE, providersSet.size(), "all generated items has unique id");
    }
}
