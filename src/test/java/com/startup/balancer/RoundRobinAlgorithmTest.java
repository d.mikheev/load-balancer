package com.startup.balancer;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RoundRobinAlgorithmTest {

    private RoundRobinAlgorithm roundRobinAlgorithm = new RoundRobinAlgorithm();

    @Test
    public void get_whenNoProviders_shouldThrowIllegalArgumentException() {
        // given
        List<Provider> inputProviders = Collections.emptyList();

        // when
        var exception = Assertions.assertThrows(IllegalArgumentException.class, () -> roundRobinAlgorithm.get(inputProviders));

        // then
        Assertions.assertEquals("There are no providers to invoke", exception.getMessage());
    }

    @Test
    public void get_shouldGetSequentialProviderId() {
        // given
        List<Provider> inputProviders = Stream.generate(Provider::new)
                .limit(3L)
                .collect(Collectors.toList());

        // when
        var sequentialId1 = roundRobinAlgorithm.get(inputProviders);
        var sequentialId2 = roundRobinAlgorithm.get(inputProviders);
        var sequentialId3 = roundRobinAlgorithm.get(inputProviders);
        var sequentialId4 = roundRobinAlgorithm.get(inputProviders);
        var sequentialId5 = roundRobinAlgorithm.get(inputProviders);
        var sequentialId6 = roundRobinAlgorithm.get(inputProviders);
        var sequentialId7 = roundRobinAlgorithm.get(inputProviders);

        // then
        Assertions.assertAll("Check execution order:",
                () -> {
                    Assertions.assertEquals(inputProviders.get(0).getId(), sequentialId1);
                    Assertions.assertEquals(inputProviders.get(1).getId(), sequentialId2);
                    Assertions.assertEquals(inputProviders.get(2).getId(), sequentialId3);
                    Assertions.assertEquals(inputProviders.get(0).getId(), sequentialId4);
                    Assertions.assertEquals(inputProviders.get(1).getId(), sequentialId5);
                    Assertions.assertEquals(inputProviders.get(2).getId(), sequentialId6);
                    Assertions.assertEquals(inputProviders.get(0).getId(), sequentialId7);
                }
        );
    }
}
