package com.startup.balancer;

public class NumberOfParallelCalsExceededException extends RuntimeException {
    NumberOfParallelCalsExceededException(int numberOfParallelCalls) {
        super(String.format("Maximum number %d of concurrent calls exceeded", numberOfParallelCalls));
    }
}
