package com.startup.balancer;

import java.util.List;
import java.util.Random;

public class RandomAlgorithm implements LoadBalancingAlgorithm {

    private final Random random = new Random();

    @Override
    public String get(List<Provider> providers) {
        if (providers.size() == 0) {
            throw new IllegalArgumentException("There are no providers to invoke");
        }
        var randomIndex = random.nextInt(providers.size());
        return providers.get(randomIndex).get();
    }
}
