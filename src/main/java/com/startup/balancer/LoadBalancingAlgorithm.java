package com.startup.balancer;

import java.util.List;

public interface LoadBalancingAlgorithm {

    public String get(List<Provider> providers);
}
