package com.startup.balancer;

import java.util.List;

public class RoundRobinAlgorithm implements LoadBalancingAlgorithm {

    private int counter = 0;

    public RoundRobinAlgorithm() {
    }

    @Override
    public String get(List<Provider> providers) {
        if (providers.size() == 0) {
            throw new IllegalArgumentException("There are no providers to invoke");
        }

        if (counter >= providers.size()) {
            counter = 0;
        }
        Provider provider = providers.get(counter);
        counter += 1;
        return provider.get();
    }
}
