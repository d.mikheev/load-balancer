package com.startup.balancer;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

public class LoadBalancer {
    private static final int DEFAULT_NUMBER_OF_PARALLEL_CALLS = 1;
    private static final int DEFAULT_HEARTBEAT_INTERVAL_SEC = 1;
    private static final int PROVIDERS_MAX_COUNT = 10;

    private final List<Provider> providers = new ArrayList<>(PROVIDERS_MAX_COUNT);
    private Semaphore semaphore;
    private final int maxNumberOfParallelCalls;
    private ReentrantLock lock = new ReentrantLock();
    private final LoadBalancingAlgorithm algorithm;
    private final HeartBeatChecker heartBeatChecker;

    public LoadBalancer() {
        this.heartBeatChecker = new HeartBeatChecker(this, DEFAULT_HEARTBEAT_INTERVAL_SEC);
        this.algorithm = new RandomAlgorithm();
        this.maxNumberOfParallelCalls = DEFAULT_NUMBER_OF_PARALLEL_CALLS;
        this.semaphore = new Semaphore(DEFAULT_NUMBER_OF_PARALLEL_CALLS);
    }

    public LoadBalancer(int heartBeatIntervalSec, int maxNumberOfParallelCalls, LoadBalancingAlgorithm algorithm) {
        this.heartBeatChecker = new HeartBeatChecker(this, heartBeatIntervalSec);
        this.maxNumberOfParallelCalls = maxNumberOfParallelCalls;
        this.algorithm = algorithm;
        this.semaphore = new Semaphore(maxNumberOfParallelCalls);
    }

    boolean include(Provider provider) {
        System.out.println("Provider included " + provider.getId());
        lock.lock();
        try {
            semaphore.release(maxNumberOfParallelCalls);
            return register(List.of(provider));
        } finally {
            lock.unlock();
        }
    }

    boolean exclude(Provider provider) {
        if (provider == null) {
            throw new IllegalArgumentException("Excluded provider can not be null");
        }
        System.out.println("Provider excluded " + provider.getId());
        lock.lock();
        try {
            semaphore.acquireUninterruptibly(maxNumberOfParallelCalls);
            return providers.remove(provider);
        } finally {
            lock.unlock();
        }
    }

    public String get() {
        if (!semaphore.tryAcquire()) {
            throw new NumberOfParallelCalsExceededException(maxNumberOfParallelCalls);
        }
        try {
            return algorithm.get(this.providers);
        } finally {
            semaphore.release();
        }
    }

    public boolean register(List<Provider> providersToRegister) {
        if (providersToRegister == null) {
            throw new IllegalArgumentException("Register providers can not be null");
        }
        lock.lock();
        try {
            var providersSummary = providers.size() + providersToRegister.size();
            if (providersSummary <= PROVIDERS_MAX_COUNT) {
                var success = providers.addAll(providersToRegister);
                semaphore = new Semaphore(providers.size() * maxNumberOfParallelCalls);
                return success;
            } else {
                throw new IllegalStateException(
                        String.format("Trying to register %d providers, balancer already contains %d providers, limit is %d",
                                providersToRegister.size(),
                                providers.size(),
                                PROVIDERS_MAX_COUNT));
            }
        } finally {
            lock.unlock();
        }
    }

    public List<String> getProvidersIds() {
        return providers.stream().map(Provider::getId).collect(Collectors.toList());
    }

    List<Provider> getProviders() {
        return new ArrayList<>(providers);
    }
}
