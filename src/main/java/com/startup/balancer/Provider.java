package com.startup.balancer;

import java.util.Objects;
import java.util.Random;
import java.util.UUID;

class Provider {
    private final Random random = new Random();

    private final String id;

    public Provider() {
        this.id = UUID.randomUUID().toString();
    }

    String get() {
        return id;
    }

    boolean check() {
        return random.nextBoolean();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Provider provider = (Provider) o;
        return Objects.equals(id, provider.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public String getId() {
        return id;
    }
}
