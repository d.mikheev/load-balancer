package com.startup.balancer;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;

public class HeartBeatChecker {

    public static final int DEFAULT_DELAY_MS = 500;
    private final LoadBalancer loadBalancer;
    private final ConcurrentHashMap<Provider, Integer> heartbeatExcludedProvidersSuccessfulCallsMap;
    private final int heartBeatRateSec;
    private final Timer timer = new Timer();

    public HeartBeatChecker(LoadBalancer loadBalancer, int heartBeatRateSec) {
        this.loadBalancer = loadBalancer;
        this.heartbeatExcludedProvidersSuccessfulCallsMap = new ConcurrentHashMap<>();
        this.heartBeatRateSec = heartBeatRateSec;
        start();
    }

    private void start() {
        this.timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                System.out.println("Current size = " + loadBalancer.getProviders().size());
                heartbeatExcludedProvidersSuccessfulCallsMap.entrySet().forEach(
                        providerCallsEntry -> {
                            if (providerCallsEntry.getKey().check()) {
                                if (providerCallsEntry.getValue() == 1) {
                                    loadBalancer.include(providerCallsEntry.getKey());
                                    heartbeatExcludedProvidersSuccessfulCallsMap.remove(providerCallsEntry.getKey());
                                } else {
                                    providerCallsEntry.setValue(1);
                                }
                            } else {
                                providerCallsEntry.setValue(0);
                            }
                        }
                );

                loadBalancer.getProviders().forEach(
                        provider -> {
                            if (!provider.check()) {
                                loadBalancer.exclude(provider);
                                heartbeatExcludedProvidersSuccessfulCallsMap.put(provider, 0);
                            }
                        }
                );


            }
        }, DEFAULT_DELAY_MS, heartBeatRateSec * 1000L);
    }

}
