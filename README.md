# Load-balancer
balancer package contains components:
* **LoadBalancer** - component that distributes incoming requests to a list of registered providers
* **HeartBeatChecker** - component encapsulating heartbeat logic
* **RandomAlgorithm** - component encapsulating random invocation strategy in load balancer
* **RoundRobinAlgorithm** - component encapsulating round robin invocation strategy in load balancer
* **Provider** - component provider

#### Running the tests
1. Download or clone the project.
2. Change to the root project directory.
3. Run tests using maven:
```bash
mvn test
```
